package Klasusur2;

import java.util.Random;





public class Klausur2 {
	public final int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		Klausur2 k2 = new Klausur2();
		int n = 1000; // Anzahl der Stellen

		// Erstellt und sortiert Array mit n Stellen
		long[] zahlen = k2.getSortedIntArray(n);
		Stoppuhr su = new Stoppuhr();
		su.start();
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen.length);
			break;
			}
		su.stopp();
		System.out.println(n);
		System.out.println(su.getDauer() + "Nanos");
		
		

	}

	public int lineareSuche(long[] zahlen, long gesuchteZahl) {
		for (int i = 0; i < zahlen.length; i++)
			if (zahlen[i] == gesuchteZahl)
				return i;
		return NICHT_GEFUNDEN;
	}

	public int schlaubiSchlumpfSuche(long[] zahlen, long gesuchteZahl) {
		int l = zahlen.length / 4 - 1;
		while (l < zahlen.length && l >= 0) {
			if (gesuchteZahl == zahlen[l])
				return l;
			if (gesuchteZahl < zahlen[l])
				l--;
			else
				l = l + zahlen.length / 4;
		}
		return NICHT_GEFUNDEN;
	}

	public int binaereSuche(long[] zahlen, long gesuchteZahl) {
		int l = 0, r = zahlen.length - 1;
		int m;

		while (l <= r) {
			// Bereich halbieren
			m = l + ((r - l) / 2);

			if (zahlen[m] == gesuchteZahl) // Element gefunden?
				return m;

			if (zahlen[m] > gesuchteZahl)
				r = m - 1; // im linken Abschnitt weitersuchen
			else
				l = m + 1; // im rechten Abschnitt weitersuchen
		}
		return NICHT_GEFUNDEN;
	}

	// -----------------------Finger weg von diesem Teil des Codes

	/**
	 * Methode f�llt das Attribut zahlen mit einem Array der L�nge des Paramaters
	 * z.B. getIntArray(1000) gibt ein 1000-stelliges Array zur�ck
	 * 
	 * @param stellen
	 *            - Anzahl der zuf�lligen Stellen
	 * @return das Array
	 */
	public long[] getSortedIntArray(int stellen) {
		Random rand = new Random(666L);
		long[] zahlen = new long[stellen];
		for (int i = 0; i < stellen; i++)
			zahlen[i] = rand.nextInt(20000000);
		this.radixSort(zahlen);
		return zahlen;
	}

	/**
	 * Super effizienter Algorithmus zur Sortierung der Zahlen Laufzeit von
	 * //NoHintAvailable
	 */
	public void radixSort(long[] zahlen) {
		long[] temp = new long[20000000];
		for (long z : zahlen)
			temp[(int) z]++;
		int stelle = 0;
		for (int i = 0; i < temp.length; i++)
			if (temp[i] != 0)
				while (temp[i]-- != 0)
					zahlen[stelle++] = i;
	}

}
