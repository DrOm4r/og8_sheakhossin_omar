package Klasusur2;

public class Stoppuhr {

	private long startzeit;
	private long endzeit;

	public void start() {
		this.startzeit = System.nanoTime();
	}

	public void stopp() {
		this.endzeit = System.nanoTime();
	}

	public void reset() {
		this.startzeit = 0;
		this.endzeit = 0;

	}

	public long getDauer() {
		return endzeit - startzeit;
	}
}
