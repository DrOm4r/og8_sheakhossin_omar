package Zahlensortierer;

public class Bubblesort {
	public static int[] bubblesort(int[] a) {
		int temp;
		int n = 0;
		for (int i = 1; i < a.length; i++) {
			for (int j = 0; j < a.length - i; j++) {
				if (a[j] > a[j + 1]) {
					n = n + 1;
					temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
				}

			}
		}
		System.out.println(n);
		return a;
		
	}

	public static void main(String[] args) {
		int[] b = { 1, 87, 52, 78, 95, 75, 12, 35, 47, 54, 45, 14, 5, 8, 2, 7, 4 };
		int[] c = bubblesort(b);
		
		for (int i = 0; i < c.length; i++) {
			System.out.print(c[i] + ",");
		}

	}

}
