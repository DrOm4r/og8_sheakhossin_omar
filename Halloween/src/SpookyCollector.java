import java.util.*;
public class SpookyCollector implements Comparable{
	boolean waffenandrohung;
	  int spruch;
	  String temperament;
	  String type;
	  int sammelleistung;
	  Random r = new Random();

	  static String[] types = { "Verfluchte Hexe", "Motivierter Werwolf", "Verst�mmelter Untoter", "Verspielter Purger" };
	  static String[] sprueche = {
	      "Wenn der Werwolf zum Feste heult, uns es �ngstlich in der Magengrube greult. Denn zum Feste sind geladen, alle b�sen Schurken und falschen Hasen.",
	      "Gruseln, Spuken und Erschrecken, wo gemeine Geister sich ihre Finger nach deinen Energien lecken. Wo Hexen, Gespenster und ihre Genossen Fl�che und Verhexungen haben auf dich geschossen.",
	      "Trick or Treat", "S��es oder es gibt Saures", "*Willk�rliches Ger�chel*" };
	  static String[] temperamente = { "martialisch", "dramatisch", "hysterisch", "biestig", "freundlich", "phlegmatisch",
	      "untot" };
	    
	  public SpookyCollector() {
	    type = types[r.nextInt(4)];
	    spruch = r.nextInt(5);
	    waffenandrohung = r.nextBoolean();
	    temperament = temperamente[r.nextInt(7)];
	    sammelleistung = r.nextInt(100) + 1;
	  }

	  public boolean isWaffenandrohung() {
	    return waffenandrohung;
	  }

	  public String getTemperament() {
	    return temperament;
	  }

	  public static String[] getSprueche() {
	    return sprueche;
	  }

	  public int getSammelleistung() {
	    return this.sammelleistung;
	  }

	  public String getType() {
	    return type;
	  }

	  public int getSpruch() {
	    return spruch;
	  }

	  @Override
	  public int compareTo(Object o) {
	    return sammelleistung - ((SpookyCollector) o).getSammelleistung();
	  }
}
