
public class HalloweenTest {
	public static void main(String[] args) {
	    Halloween h = new Halloween();
	    h.fillTowns();
	    
	    // Wieviele �berl�ufer gibt es?
	    System.out.println("\n***************************************************");
	    System.out.println("*                   Halloween                     *");
	    System.out.println("***************************************************\n");
	    
	    System.out.println("Es gibt " + h.countRenegades() + " �berl�ufer, um die wir uns k�mmern m�ssen.\n");
	    
	    // �berl�ufer massakrieren
	    h.renegade();
	    System.out.println("(x_x).\nErledigt. Es gibt noch " + h.sleepyhollow.size() + " J�ger und Sammler.\n");
	    
	    // Welcher Spruch war der erfolgreichste?
	    System.out.println("Der erfolgreichste Spruch war: \"" + h.success("Spruch") + "\"");
	    System.out.println();
	    
	    // Welcher Typ war das erfolgreichste?
	    System.out.println("Der erfolgreichste Typ war: " + h.success("Typ"));
	    System.out.println();
	    
	    // Sollte zur Gewinnmaximierung Waffengewalt angewendet werden?
	    System.out.print("Mit oder ohne Waffen�berzeugung? ");
	    System.out.println(h.success("Waffengewalt"));
	    System.out.println();
	    
	    // Rankingliste der Top 100
	    System.out.println("\nUnd hier die Besten 100: ");
	    h.ranking();
	    
	    // Besten SpookyCollector aus Set entfernen
	    SpookyCollector best = h.killBest();
	    System.out.println("\nB�sewicht " + best.getType() + " mit " + best.getSammelleistung()
	    + " Candys im K�rbchen erfolgreich get�tet.");
	    
	    // Schlechte 100 SpookyCollectors aus Set entfernen
	    h.sackLosers();
	    System.out.println(h.tset.size());
	  }
}
