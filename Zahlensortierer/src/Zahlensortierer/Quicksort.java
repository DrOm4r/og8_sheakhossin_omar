package Zahlensortierer;

import Klasusur2.Stoppuhr;

public class Quicksort {
	public static int[] intArr = { 15, 8, 2, 7, 1, 4, 9, 6, 12 };
	public int[] sort(int l, int r) {
		int q;
		if (l < r) {
			q = partition(l, r);
			sort(l, q);
			sort(q + 1, r);
		}
		return intArr;
	
	}

	int partition(int l, int r) {

		int i, j, x = intArr[(l + r) / 2];
		i = l - 1;
		j = r + 1;
		while (true) {
			do {
				i++;
			} while (intArr[i] < x);

			do {
				j--;
			} while (intArr[j] > x);

			if (i < j) {
				int k = intArr[i];
				intArr[i] = intArr[j];
				intArr[j] = k;
			} else {
				return j;
			}
		}
	}

	public static void main(String[] args) {
		Stoppuhr su = new Stoppuhr();
		su.start();
		System.out.println("Quicksort:");
		System.out.println("-----------");
		System.out.println("Start: ");
		System.out.println("-----------");
		
		Quicksort qs = new Quicksort();
		int[] arr = qs.sort(0, intArr.length - 1);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(intArr[i] + ",");
		}
		su.stopp();
		System.out.println(" ");
		System.out.println("-----------");
		System.out.println("Ende: ");
		System.out.println("-----------");
		System.out.println("Dauer: " + su.getDauer() + " nanos ");
	}
}
