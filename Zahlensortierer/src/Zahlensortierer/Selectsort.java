package Zahlensortierer;

import Klasusur2.Stoppuhr;

public class Selectsort {
	public static void main(String[] args) {
		Stoppuhr su = new Stoppuhr();
		su.start();
		int[] unsortiert = { 15, 8, 2, 7, 1, 4, 9, 6, 12 };
		int[] sortiert = selectionsort(unsortiert);
		System.out.println("Selectsort: ");
		System.out.println("-----------");
		System.out.println("Start: ");
		System.out.println("-----------");
		
		for (int i = 0; i < sortiert.length; i++) {
			System.out.print(sortiert[i] + ", ");
		}
		su.stopp();
		
		System.out.println("");
		System.out.println("-----------");
		System.out.println("Ende: ");
		System.out.println("-----------");
		System.out.println("Dauer: " + su.getDauer() + " nanos ");
	}

	public static int[] selectionsort(int[] sortieren) {
		for (int i = 0; i < sortieren.length - 1; i++) {
			for (int j = i + 1; j < sortieren.length; j++) {
				if (sortieren[i] > sortieren[j]) {
					int temp = sortieren[i];
					sortieren[i] = sortieren[j];
					sortieren[j] = temp;
				}
			}
		}

		return sortieren;
	}
}
