import java.util.*;

public class KeyStore03 {
	private static LinkedList<String> l = new LinkedList<>();
	private static int i = 0;

	public boolean add(String eintrag) {
		if (i < 100) {
			l.add(eintrag);
			i++;
			return true;
		} else {
			return false;
		}
	}

	public int indexOf(String eintrag) {
		return l.indexOf(eintrag);
	}

	public boolean remove(int index) {
		if (i >= 0) {
			l.remove(i);
			return true;
		}
		return false;

	}

	public String toString() {
		return l.toString();
	}
}
