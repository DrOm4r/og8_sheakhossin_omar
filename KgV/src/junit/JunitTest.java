package junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.*;

import Data.KgV;


class JunitTest {

	@Test
	public void test1() {
		assertEquals(0, KgV.kgv(0, 0));
	}

	public void test2() {
		assertEquals(18, KgV.kgv(1, 18));
	}

	public void test3() {
		assertEquals(8, KgV.kgv(8, 8));
	}

	public void test4() {
		assertEquals(-90, KgV.kgv(-5, 18));
	}

	public void test5() {
		assertEquals(-12, KgV.kgv(-4, -3));
	}

	public void test6() {
		assertEquals(4770, KgV.kgv(954, 5));
	}

	public void test0() {
		fail("Not yet implemented");
	}

}
