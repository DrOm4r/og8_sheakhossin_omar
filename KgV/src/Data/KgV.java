package Data;

public class KgV {
	public static void main(String[] args) {
		args = new String[] { "8", "18" };

		if (args.length != 2) {
			System.out.println("Benutzung: java KGV n1 n2");
			System.exit(-1);
		}

		int x = 0, y = 0;
		try {
			x = Integer.valueOf(args[0]);
			y = Integer.valueOf(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Eines der Argumente oder beide Argumente bestehen" + "nicht aus einer Zahl");
		}

		System.out.println("KGV von " + args[0] + " und " + args[1] + ": " + kgv(x, y));
	}

	public static int kgv(int x, int y) {
		if (x < 0 || y < 0)
			return -1;
		if (x == y)
			return x;
		if (x == 1)
			return y;
		if (y == 1)
			return x;

		int x1 = x, y1 = y;
		while (x != y) {
			if (x < y)
				x += x1;
			else
				y += y1;
		}

		return x;
	}

}
