package ZooTycoon;

public class Addon {

	private String bezeichnung;
	private int idNummer;
	private double preis;
	private double bestand;
	
	public Addon(String bezeichnung , int idNummer , double preis , double bestand) {
	this.bezeichnung = bezeichnung;
	this.idNummer = idNummer;
	this.preis = preis;
	this.bestand = bestand;
			
	}
	

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public double getBestand() {
		return bestand;
	}

	public void setBestand(double bestand) {
		this.bestand = bestand;
	}

}
