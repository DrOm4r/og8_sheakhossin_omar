import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DateiVerwalter {
	private File file;

	public DateiVerwalter(File file) {
		this.file = file;
	}

	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (IOException e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
	}

	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			System.out.println("Datei wurde nicht gefunden");
			e.printStackTrace();
		}

	}

	public static boolean isPrim(long zahl) {
		zahl = Math.abs(zahl);
		if (zahl < 2)
			return false;
		double sqrt = Math.sqrt(zahl);
		for (long teiler = 2; teiler <= sqrt; teiler++) {
			if (zahl % teiler == 0) {
				
				return false;
			}
		}
		return true;
	}

	public static int rakam(int zahl) {
		zahl = Math.abs(zahl);
		double sqrt = Math.sqrt(zahl);
		for (long teiler = 2; teiler <= sqrt; teiler++) {
			if (zahl % teiler == 0) {
			}
		}
		return zahl;
	}

	public static void main(String[] args) {
		File file = new File("Primzahlen.txt");
		DateiVerwalter dv = new DateiVerwalter(file);
		int n = 1;
		int i = 0;
		while (n <= 1000000) {
			i++;
			if ((dv.isPrim(i)) == true) {
				int broj = dv.rakam(i);
				String s = Integer.toString(broj);
				dv.schreibe(s);
				n++;
			}
		}

	}
}
